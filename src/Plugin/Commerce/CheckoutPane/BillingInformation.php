<?php

namespace Drupal\commerce_shipping_same_as_billing\Plugin\Commerce\CheckoutPane;

use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\BillingInformation as BaseBillingInformation;

/**
 * Overrides the default BillingInformation pane.
 *
 * Adds the edited billing_profile to the form state so we can use this profile
 * in our own ProfileFieldCopy service.
 */
class BillingInformation extends BaseBillingInformation {

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $pane_form = parent::buildPaneForm($pane_form, $form_state, $complete_form);

    /** @var \Drupal\commerce\Plugin\Commerce\InlineForm\EntityInlineFormInterface $inline_form */
    $inline_form = $pane_form['profile']['#inline_form'];

    if (!$form_state->has('billing_profile')) {
      $form_state->set('billing_profile', $inline_form->getEntity());
    }

    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    parent::validatePaneForm($pane_form, $form_state, $complete_form);
    /** @var \Drupal\commerce\Plugin\Commerce\InlineForm\EntityInlineFormInterface $inline_form */
    $inline_form = $pane_form['profile']['#inline_form'];
    /** @var \Drupal\profile\Entity\ProfileInterface $profile */
    $form_state->set('billing_profile', $inline_form->getEntity());
  }

}
