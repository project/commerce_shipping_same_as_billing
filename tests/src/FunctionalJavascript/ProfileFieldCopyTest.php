<?php

namespace Drupal\Tests\commerce_shipping_same_as_billing\FunctionalJavascript;

use Drupal\Core\Url;
use Drupal\Tests\commerce\FunctionalJavascript\CommerceWebDriverTestBase;
use Drupal\commerce_checkout\Entity\CheckoutFlow;
use Drupal\commerce_order\Entity\OrderType;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductType;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\field\Entity\FieldConfig;

// cspell:ignore Sentier Centarro

/**
 * Tests the "Shipping information" checkout pane.
 *
 * @group commerce_shipping_same_as_billing
 */
class ProfileFieldCopyTest extends CommerceWebDriverTestBase {

  /**
   * A French address.
   *
   * @var array
   */
  protected $frenchAddress = [
    'country_code' => 'FR',
    'locality' => 'Paris',
    'postal_code' => '75002',
    'address_line1' => '38 Rue du Sentier',
    'given_name' => 'Leon',
    'family_name' => 'Blum',
  ];

  /**
   * A US address.
   *
   * @var array
   */
  protected $usAddress = [
    'country_code' => 'US',
    'administrative_area' => 'SC',
    'locality' => 'Greenville',
    'postal_code' => '29616',
    'address_line1' => '9 Drupal Ave',
    'given_name' => 'Bryan',
    'family_name' => 'Centarro',
  ];

  /**
   * First sample product.
   *
   * @var \Drupal\commerce_product\Entity\ProductInterface
   */
  protected $firstProduct;

  /**
   * Second sample product.
   *
   * @var \Drupal\commerce_product\Entity\ProductInterface
   */
  protected $secondProduct;

  /**
   * A sample order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The shipping order manager.
   *
   * @var \Drupal\commerce_shipping\ShippingOrderManagerInterface
   */
  protected $shippingOrderManager;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce_payment_example',
    'commerce_promotion',
    'commerce_tax',
    'commerce_checkout',
    'commerce_payment',
    'commerce_product',
    'commerce_shipping_same_as_billing',
    'telephone',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getAdministratorPermissions(): array {
    return array_merge([
      'administer commerce_order',
      'administer commerce_shipment',
      'access commerce_order overview',
    ], parent::getAdministratorPermissions());
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->store->set('billing_countries', ['FR', 'US']);
    $this->store->save();

    // Turn off verification via external services.
    $tax_number_field = FieldConfig::loadByName('profile', 'customer', 'tax_number');
    $tax_number_field->setSetting('verify', FALSE);
    $tax_number_field->save();

    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = PaymentGateway::create([
      'id' => 'cod',
      'label' => 'Manual',
      'plugin' => 'manual',
      'configuration' => [
        'display_label' => 'Cash on delivery',
        'instructions' => [
          'value' => 'Sample payment instructions.',
          'format' => 'plain_text',
        ],
      ],
    ]);
    $payment_gateway->save();

    $variation_type = ProductVariationType::load('default');
    $variation_type->setTraits(['purchasable_entity_shippable']);
    $variation_type->save();

    $order_type = OrderType::load('default');
    $order_type->setThirdPartySetting('commerce_checkout', 'checkout_flow', 'shipping');
    $order_type->setThirdPartySetting('commerce_shipping', 'shipment_type', 'default');
    $order_type->save();

    // Create the order field.
    $field_definition = commerce_shipping_build_shipment_field_definition($order_type->id());
    $this->container->get('commerce.configurable_field_manager')->createField($field_definition);

    // Install the variation trait.
    $trait_manager = $this->container->get('plugin.manager.commerce_entity_trait');
    $trait = $trait_manager->createInstance('purchasable_entity_shippable');
    $trait_manager->installTrait($trait, 'commerce_product_variation', 'default');

    // Create a non-shippable product/variation type set.
    $variation_type = ProductVariationType::create([
      'id' => 'digital',
      'label' => 'Digital',
      'orderItemType' => 'default',
      'generateTitle' => TRUE,
    ]);
    $variation_type->save();

    $product_type = ProductType::create([
      'id' => 'Digital',
      'label' => 'Digital',
      'variationType' => $variation_type->id(),
    ]);
    $product_type->save();

    // Create two products. One shippable, one non-shippable.
    $variation = $this->createEntity('commerce_product_variation', [
      'type' => 'default',
      'sku' => strtolower($this->randomMachineName()),
      'price' => [
        'number' => '7.99',
        'currency_code' => 'USD',
      ],
    ]);
    /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
    $this->firstProduct = $this->createEntity('commerce_product', [
      'type' => 'default',
      'title' => 'Conference hat',
      'variations' => [$variation],
      'stores' => [$this->store],
    ]);

    $variation = $this->createEntity('commerce_product_variation', [
      'type' => 'digital',
      'sku' => strtolower($this->randomMachineName()),
      'price' => [
        'number' => '8.99',
        'currency_code' => 'USD',
      ],
    ]);
    /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
    $this->secondProduct = $this->createEntity('commerce_product', [
      'type' => 'digital',
      'title' => 'Conference ticket',
      'variations' => [$variation],
      'stores' => [$this->store],
    ]);

    $order_item = $this->createEntity('commerce_order_item', [
      'type' => 'default',
      'title' => $this->randomMachineName(),
      'quantity' => 1,
      'unit_price' => new Price('7.99', 'USD'),
      'purchased_entity' => $this->firstProduct->getDefaultVariation(),
    ]);
    $order_item->save();
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $this->order = $this->createEntity('commerce_order', [
      'type' => 'default',
      'order_number' => '2020/01',
      'store_id' => $this->store,
      'uid' => $this->adminUser->id(),
      'order_items' => [$order_item],
      'state' => 'draft',
      'payment_gateway' => $payment_gateway->id(),
    ]);

    /** @var \Drupal\commerce_shipping\Entity\PackageType $package_type */
    $package_type = $this->createEntity('commerce_package_type', [
      'id' => 'package_type_a',
      'label' => 'Package Type A',
      'dimensions' => [
        'length' => '20',
        'width' => '20',
        'height' => '20',
        'unit' => 'mm',

      ],
      'weight' => [
        'number' => '20',
        'unit' => 'g',
      ],
    ]);
    $this->container->get('plugin.manager.commerce_package_type')->clearCachedDefinitions();

    $shipping_method = $this->createEntity('commerce_shipping_method', [
      'name' => 'Standard shipping',
      'stores' => [$this->store->id()],
      // Ensure that Standard shipping shows before overnight shipping.
      'weight' => -10,
      'plugin' => [
        'target_plugin_id' => 'flat_rate',
        'target_plugin_configuration' => [
          'rate_label' => 'Standard shipping',
          'rate_amount' => [
            'number' => '9.99',
            'currency_code' => 'USD',
          ],
        ],
      ],
    ]);

    $this->shippingOrderManager = $this->container->get('commerce_shipping.order_manager');

    $checkout_flow = CheckoutFlow::load('shipping');
    $checkout_flow_configuration = $checkout_flow->get('configuration');
    $checkout_flow_configuration['panes']['shipping_information']['auto_recalculate'] = FALSE;
    $checkout_flow_configuration['panes']['shipping_information']['weight'] = 10;
    $checkout_flow->set('configuration', $checkout_flow_configuration);
    $checkout_flow->save();
  }

  /**
   * Tests checkout.
   */
  public function testCheckout(): void {
    $first_address_book_profile = $this->createEntity('profile', [
      'type' => 'customer',
      'uid' => $this->adminUser->id(),
      'address' => $this->frenchAddress,
    ]);
    $second_address_book_profile = $this->createEntity('profile', [
      'type' => 'customer',
      'uid' => $this->adminUser->id(),
      'address' => $this->usAddress,
    ]);
    $shipping_prefix = 'shipping_information[shipping_profile]';

    $this->drupalGet(Url::fromRoute('commerce_checkout.form', [
      'commerce_order' => $this->order->id(),
    ]));
    $this->assertSession()->pageTextContains('Shipping information');
    $this->assertRenderedAddress($this->frenchAddress);

    $this->assertSession()->pageTextContains('Payment information');
    $this->getSession()->getPage()->fillField('payment_information[billing_information][select_address]', $second_address_book_profile->id());
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->assertSession()->checkboxNotChecked($shipping_prefix . '[copy_fields][enable]');
    $this->getSession()->getPage()->checkField($shipping_prefix . '[copy_fields][enable]');
    $this->assertSession()->assertWaitOnAjaxRequest();

    // Confirm that the shipping fields were copied on page submit.
    $this->submitForm([], 'Continue to review');
    $this->order = $this->reloadEntity($this->order);
    $profiles = $this->order->collectProfiles();
    $shipping_profile = $profiles['shipping'];
    $this->assertNotEmpty($shipping_profile);
    /** @var \Drupal\address\AddressInterface $address */
    $address = $shipping_profile->get('address')->first();
    $this->assertEquals($this->usAddress, array_filter($address->toArray()));
    $this->assertNotEmpty($shipping_profile->getData('copy_fields'));
    $this->assertEmpty($shipping_profile->getData('copy_to_address_book'));

    // Go back, and edit the shipping profile. Confirm changes are carried over.
    $this->clickLink('Go back');
    $this->assertSession()->checkboxChecked($shipping_prefix . '[copy_fields][enable]');
    $this->getSession()->getPage()->pressButton('billing_edit');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->submitForm([
      'payment_information[billing_information][address][0][address][postal_code]' => '75003',
    ], 'Continue to review');

    $expected_address = [
      'postal_code' => '75003',
    ] + $this->usAddress;
    $this->assertRenderedAddress($expected_address, $shipping_profile->id());
    /** @var \Drupal\profile\Entity\ProfileInterface $shipping_profile */
    $shipping_profile = $this->reloadEntity($shipping_profile);
    $address = $shipping_profile->get('address')->first();
    $this->assertEquals($expected_address, array_filter($address->toArray()));
    $this->assertNotEmpty($shipping_profile->getData('copy_fields'));
    $this->assertEmpty($shipping_profile->getData('copy_to_address_book'));

    // Confirm that copy_fields can be unchecked, showing the address book.
    $this->clickLink('Go back');
    $this->assertSession()->checkboxChecked($shipping_prefix . '[copy_fields][enable]');
    $this->getSession()->getPage()->uncheckField($shipping_prefix . '[copy_fields][enable]');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $options = $this->xpath('//select[@name="' . $shipping_prefix . '[select_address]"]/option');
    $this->assertCount(4, $options);
    $this->assertEquals($first_address_book_profile->id(), $options[0]->getValue());
    $this->assertEquals($second_address_book_profile->id(), $options[1]->getValue());
    $this->assertEquals('_original', $options[2]->getValue());
    $this->assertEquals('_new', $options[3]->getValue());

    // Confirm that a different profile can be selected.
    $this->getSession()->getPage()->fillField($shipping_prefix . '[select_address]', $second_address_book_profile->id());
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->submitForm([], 'Continue to review');

    /** @var \Drupal\profile\Entity\ProfileInterface $shipping_profile */
    $shipping_profile = $this->reloadEntity($shipping_profile);
    $address = $shipping_profile->get('address')->first();
    $this->assertEquals($this->usAddress, array_filter($address->toArray()));
    $this->assertEmpty($shipping_profile->getData('copy_fields'));
    $this->assertEquals($second_address_book_profile->id(), $shipping_profile->getData('address_book_profile_id'));

    // Confirm that the copy_fields checkbox is still unchecked.
    $this->clickLink('Go back');
    $this->saveHtmlOutput();
    $this->assertSession()->checkboxNotChecked($shipping_prefix . '[copy_fields][enable]');
    $this->assertRenderedAddress($this->usAddress, $shipping_profile->id());
    // Confirm that the _original option is no longer present.
    $options = $this->xpath('//select[@name="' . $shipping_prefix . '[select_address]"]/option');
    $this->assertCount(3, $options);
    $this->assertEquals($first_address_book_profile->id(), $options[0]->getValue());
    $this->assertEquals($second_address_book_profile->id(), $options[1]->getValue());
    $this->assertEquals('_new', $options[2]->getValue());
  }

  /**
   * Test this module without commerce_payment enabled.
   */
  public function testCheckoutWithoutPayment(): void {
    $this->order->set('payment_gateway', NULL);
    $this->order->save();
    $this->container->get('module_installer')->uninstall(['commerce_payment'], TRUE);
    $this->container = $this->kernel->rebuildContainer();

    $first_address_book_profile = $this->createEntity('profile', [
      'type' => 'customer',
      'uid' => $this->adminUser->id(),
      'address' => $this->frenchAddress,
    ]);
    $second_address_book_profile = $this->createEntity('profile', [
      'type' => 'customer',
      'uid' => $this->adminUser->id(),
      'address' => $this->usAddress,
    ]);
    $shipping_prefix = 'shipping_information[shipping_profile]';

    $this->drupalGet(Url::fromRoute('commerce_checkout.form', [
      'commerce_order' => $this->order->id(),
    ]));

    $this->assertSession()->pageTextContains('Billing information');
    $this->getSession()->getPage()->fillField('billing_information[profile][select_address]', $second_address_book_profile->id());
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->assertSession()->checkboxNotChecked($shipping_prefix . '[copy_fields][enable]');
    $this->getSession()->getPage()->checkField($shipping_prefix . '[copy_fields][enable]');
    $this->assertSession()->assertWaitOnAjaxRequest();

    // Confirm that the shipping fields were copied on page submit.
    $this->submitForm([], 'Continue to review');
    $this->order = $this->reloadEntity($this->order);
    $profiles = $this->order->collectProfiles();
    $shipping_profile = $profiles['shipping'];
    $this->assertNotEmpty($shipping_profile);
    /** @var \Drupal\address\AddressInterface $address */
    $address = $shipping_profile->get('address')->first();
    $this->assertEquals($this->usAddress, array_filter($address->toArray()));
    $this->assertNotEmpty($shipping_profile->getData('copy_fields'));
    $this->assertEmpty($shipping_profile->getData('copy_to_address_book'));
  }

  /**
   * Asserts that the given address is rendered on the page.
   *
   * @param array $address
   *   The address.
   * @param string $profile_id
   *   The parent profile ID.
   */
  protected function assertRenderedAddress(array $address, $profile_id = NULL): void {
    $parent_class = $profile_id ? '.profile--' . $profile_id : '.profile';
    $page = $this->getSession()->getPage();
    $address_text = $page->find('css', $parent_class . ' p.address')->getText();
    foreach ($address as $property => $value) {
      if ($property == 'country_code') {
        $value = $this->countryList[$value];
      }
      $this->assertStringContainsString($value, $address_text);
    }
  }

}
