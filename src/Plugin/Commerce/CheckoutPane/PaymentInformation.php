<?php

namespace Drupal\commerce_shipping_same_as_billing\Plugin\Commerce\CheckoutPane;

use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce\Plugin\Commerce\InlineForm\EntityInlineFormInterface;
use Drupal\commerce_payment\Plugin\Commerce\CheckoutPane\PaymentInformation as BasePaymentInformation;

/**
 * Overrides the default BillingInformation pane.
 *
 * Adds the edited billing_profile to the form state so we can use this profile
 * in our own ProfileFieldCopy service.
 */
class PaymentInformation extends BasePaymentInformation {

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $pane_form = parent::buildPaneForm($pane_form, $form_state, $complete_form);

    $inline_form = $pane_form['add_payment_method']['billing_information']['#inline_form'] ?? $pane_form['billing_information']['#inline_form'] ?? NULL;

    if ($inline_form instanceof EntityInlineFormInterface && !$form_state->has('billing_profile')) {
      $form_state->set('billing_profile', $inline_form->getEntity());
    }

    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    parent::validatePaneForm($pane_form, $form_state, $complete_form);
    $inline_form = $pane_form['add_payment_method']['billing_information']['#inline_form'] ?? $pane_form['billing_information']['#inline_form'];
    if ($inline_form instanceof EntityInlineFormInterface) {
      $form_state->set('billing_profile', $inline_form->getEntity());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    parent::submitPaneForm($pane_form, $form_state, $complete_form);
    $form_state->set('billing_profile', $this->order->getBillingProfile());
  }

}
